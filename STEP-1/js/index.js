"use strict";

const tabs = document.getElementById("tabs");
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content > li");

function clickHandler(e) {
  let service = e.target.textContent;
  tabsTitle.forEach((e) => {
    e.classList.remove("active");
  });
  tabsContent.forEach((e) => {
    if (e.dataset.service === service) {
      e.classList.add("active");
    } else {
      e.classList.remove("active");
    }
  });
  e.target.classList.add("active");
}

tabs.addEventListener("mousedown", clickHandler);

let workTabs = document.querySelector(".work-tabs");
let workTitle = document.querySelectorAll(".work-title");
let workContent = document.querySelectorAll(".work-card");

workTabs.addEventListener("click", (e) => {
  if (e.target.classList.contains("work-title")) {
    workTitle.forEach((e) => {
      e.classList.remove("active");
    });
  }
  workContent.forEach((element) => {
    if (e.target.innerHTML === element.dataset.type) {
      element.classList.remove("filter");
      e.target.classList.add("active");
    } else if (e.target.innerHTML === "All") {
      e.target.classList.add("active");
      element.classList.remove("filter");
    } else if (e.target.classList.contains("work-title")) {
      element.classList.add("filter");
    }
  });
});

let loadMoreBtn = document.getElementById("load-more");

loadMoreBtn.addEventListener("click", (e) => {
  e.target.style = "display: none";

  if ((e.target.style = "display: none")) {
  }
  setTimeout(function timered() {
    workContent.forEach((element) => {
      element.classList.remove("hide");
    });
    if (workTitle[0].classList.contains("active")) {
      workContent.forEach((element) => {
        element.classList.remove("filter");
      });
    }
  }, 500);
});

let sliderNavigation = document.querySelector(".slider-navigation");
let slideContent = document.querySelector(".slide-content");
let clientsImages = document.querySelectorAll(".client-image");
let slides = document.querySelectorAll(".slide");

sliderNavigation.addEventListener("click", (e) => {
  if (e.target.classList.contains("client-image")) {
    clientsImages.forEach((e) => {
      e.classList.remove("active");
    });
    e.target.classList.add("active");
    slideContent.style = `transform: translate(-${
      slides[e.target.dataset.position].offsetLeft
    }px)`;
  }
});

let btnBack = document.querySelector(".button-back");
let btnForward = document.querySelector(".button-forward");

btnBack.addEventListener("click", () => {
  if (clientsImages[0].classList.contains("active")) {
    clientsImages[3].click();
  } else if (clientsImages[1].classList.contains("active")) {
    clientsImages[0].click();
  } else if (clientsImages[2].classList.contains("active")) {
    clientsImages[1].click();
  } else if (clientsImages[3].classList.contains("active")) {
    clientsImages[2].click();
  }
});

btnForward.addEventListener("click", () => {
  if (clientsImages[3].classList.contains("active")) {
    clientsImages[0].click();
  } else if (clientsImages[2].classList.contains("active")) {
    clientsImages[3].click();
  } else if (clientsImages[1].classList.contains("active")) {
    clientsImages[2].click();
  } else if (clientsImages[0].classList.contains("active")) {
    clientsImages[1].click();
  }
});
