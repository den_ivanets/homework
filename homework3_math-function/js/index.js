"use strict";

let firstNumber;
let secondNumber;

while (isNaN(firstNumber)) {
  firstNumber = +prompt("Enter the first number:");
}
while (isNaN(secondNumber)) {
  secondNumber = +prompt("Enter the second number:");
}

let action = prompt("Enter your action: +, -, *, /");

function calc(action, firstNumber, secondNumber) {
  switch (action) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
    default:
      return "Please, enter the data correctly";
  }
}
console.log(calc(action, firstNumber, secondNumber));
