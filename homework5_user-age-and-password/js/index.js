"use strict";

function createNewUser() {
  this.userName = prompt("Enter you name:");
  this.userSecondName = prompt("Enter you second name:");

  this.birthday = prompt("Enter your birthday in format dd.mm.yyyy");
  let dd = this.birthday.slice(0, 2);
  let mm = this.birthday.slice(3, 5);
  let yy = this.birthday.slice(6, 10);

  this.getLogin = function () {
    let userLogin =
      this.userName[0].toLowerCase() + this.userSecondName.toLowerCase();
    return userLogin;
  };

  this.getAge = function () {
    let today = new Date();
    let birthDate = new Date(yy, mm - 1, dd);
    let userAge = today.getFullYear() - birthDate.getFullYear();
    let month = today.getMonth() - birthDate.getMonth();
    if (month < 0) {
      userAge--;
    }
    return userAge;
  };

  this.getPassword = function () {
    let password =
      this.userName[0].toLocaleUpperCase() +
      this.userSecondName.toLowerCase() +
      yy;
    return password;
  };
}

let user1 = new createNewUser();

console.log(`${user1.getLogin()}`);
console.log(`${user1.getAge()}`);
console.log(`${user1.getPassword()}`);
