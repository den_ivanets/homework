"use strict";

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let div = document.createElement("div");
div.innerHTML = array;
document.body.prepend(div);

function getListContent() {
  let result = [];
  for (let i = 0; i <= array.length - 1; i++) {
    let li = document.createElement("li");
    li.append(array[i]);
    result.push(li);
  }
  return result;
}

div.append(...getListContent());
